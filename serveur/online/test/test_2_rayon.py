import networkx as nx
import os

# DISTANCE MAX = 198 manhatan_distance((0,0), (99,99))

output_file = "test\log_2_rayon.txt"
cout = open(output_file, 'w')

# calcul de distance de manhattan entre 2 points, un point est un tuple (x, y)
def manhatan_distance (p_1, p_2):
    return abs(p_1[0] - p_2[0]) + abs(p_1[1] - p_2[1]) 

def writeState(pos_serveurs, title = "State"):
    cout.write(title + " : \n")
    for k in pos_serveurs:
        cout.write("{} : {}\n".format(k, pos_serveurs[k]))

def count_voisin(pos_serveurs, s, rayon):
    c = 0
    for k in pos_serveurs.keys():
        if k == s:
            continue
        else:
            if manhatan_distance(pos_serveurs[k], pos_serveurs[s]) < rayon:
                c += 1
    return c

def online_k_serveurs(pos_sites, pos_serveurs, demande):

    # Verification si serveur deja sur place, on renvoi ce serveur
    for k in pos_serveurs.keys():
        if pos_serveurs[k] == pos_sites[demande]:
            return k
    
    # Recherche d'un serveur avec au moins 1 voisins dans un rayon de 3 (non isole)
    rayon = 1   # Rayon de recherche autour du site de la demande
    prox = {}   # Serveurs dans le rayon
    d_max = manhatan_distance((0,0), (99,99))   # distance max possible dans la grille
    
    # Tant que le rayon n'est pas trop grand et que tout les serveurs ne soit pas dans le rayon
    while rayon < d_max and len(prox) < len(pos_serveurs):
        l = len(prox)
        not_intersec = [i for i in pos_serveurs.keys() if i not in prox.keys()]     # liste des serveurs qui ne sont pas dans rayon
        cout.write("not_intersec : {}\n".format(not_intersec))

        # Pour tous les serveurs qui ne sont pas dans le rayon, on les ajoute a la proximite du site de la demande
        for k in not_intersec:
            if manhatan_distance(pos_serveurs[k], pos_sites[demande]) < rayon:
                prox[k] = pos_serveurs[k]

        # Si on a ajoute un ou des serveurs dans la proximite du site de la demande,
        #   on compte les voisins de ces serveurs et on retourne celui qui a au moins 1 voisin dans un rayon de 3 si il existe
        if l != len(prox):
            for k in prox.keys():
                if count_voisin(pos_serveurs, k, 1) > 0:
                    cout.write("{} a plus de 1 voisin\n".format(k))
                    return k
        
        # Si on a pas trouve de serveur non isole, on augmente de rayon de recherche
        rayon += 1
        cout.write("Rayon  = {}\n".format(rayon))
        cout.write("Prox : {}\n".format(prox))
    
    # Si pas de serveur non isole, on prend le plus proche       
    cout.write("Prend le plus proche\n")
    min_d = min([ ( manhatan_distance(pos_sites[demande], pos_serveurs[k]), k ) for k in pos_serveurs.keys()])
    cout.write("Demande : {} de position {}, serveur : {} de position {}, cout : {}\n".format(demande, pos_sites[demande], min_d[1], pos_serveurs[min_d[1]], min_d[0]))
    return min_d[1]


ratios = []
name = "instance_N400_OPT377.inst"
input_file = "public_online_dataset\\" + name

cin = open(input_file, 'r')

lines = cin.readlines()
cout_opt = int(lines[1])
k = int(lines[4])

idx_demande = lines.index('# demandes\n')
sites = {}
for i in range(7,idx_demande-1):
    coords = lines[i].split()
    sites[i-7] = (int(coords[0]), int(coords[1]))

demandes = [int(d) for d in lines[idx_demande+1].split()]
cout.write("Demandes : {}\n".format(demandes))
nb_runs = 10
meilleur_cout = float('inf')
for j in range(nb_runs):
    cout.write("\nRun {}\n".format(j))
    cout_online = 0
    serveurs = {i:(0,0) for i in range(k)} #tous les serveurs à la position 0,0 au départ
    for d in demandes:
        cout.write("\n-------demande {}\n".format(d))
        r = online_k_serveurs(sites.copy(), serveurs.copy(), d)
        writeState(serveurs)
        cout_online += manhatan_distance(serveurs[r], sites[d])
        serveurs[r] = sites[d]
    
    meilleur_cout = min(meilleur_cout, cout_online)

ratios.append(meilleur_cout/cout_opt)

cout.write("\n" + name + ': optimal: {}, online: {}, ratio: {}\n'.format(cout_opt, meilleur_cout, ratios[-1]))


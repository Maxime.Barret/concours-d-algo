import networkx as nx
import os

# calcul de distance de manhattan entre 2 points, un point est un tuple (x, y)
def manhatan_distance (p_1, p_2):
    return abs(p_1[0] - p_2[0]) + abs(p_1[1] - p_2[1]) 

output_file = "test\log_naif.txt"
cout = open(output_file, 'w')

def online_k_serveurs(pos_sites, pos_serveurs, demande):

    min_d = min([ ( manhatan_distance(pos_sites[demande], pos_serveurs[k]), k ) for k in pos_serveurs.keys()])
    cout.write("Demande : {} de position {}, serveur : {} de position {}, cout : {}\n".format(demande, pos_sites[demande], min_d[1], pos_serveurs[min_d[1]], min_d[0]))
    return min_d[1]

ratios = []
name = "instance_N400_OPT377.inst"
input_file = "public_online_dataset\\" + name

cin = open(input_file, 'r')

lines = cin.readlines()
cout_opt = int(lines[1])
k = int(lines[4])

idx_demande = lines.index('# demandes\n')
sites = {}
for i in range(7,idx_demande-1):
    coords = lines[i].split()
    sites[i-7] = (int(coords[0]), int(coords[1]))

demandes = [int(d) for d in lines[idx_demande+1].split()]
cout.write("Demandes : {}\n".format(demandes))
nb_runs = 1
meilleur_cout = float('inf')
for j in range(nb_runs):
    cout.write("\nRun {}\n".format(j))
    cout_online = 0
    serveurs = {i:(0,0) for i in range(k)} #tous les serveurs à la position 0,0 au départ
    for d in demandes:
        r = online_k_serveurs(sites.copy(), serveurs.copy(), d)
        cout_online += manhatan_distance(serveurs[r], sites[d])
        serveurs[r] = sites[d]
    
    meilleur_cout = min(meilleur_cout, cout_online)

ratios.append(meilleur_cout/cout_opt)

cout.write("\n" + name + ': optimal: {}, online: {}, ratio: {}\n'.format(cout_opt, meilleur_cout, ratios[-1]))


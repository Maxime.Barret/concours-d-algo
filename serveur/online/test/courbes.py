import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import os

nombre_fichiers = 20
nb_serveurs = []
other_dir = os.path.abspath("public_online_dataset\\")
files = []
for instance_filename in sorted(os.listdir(other_dir)):
    files.append(instance_filename)
    instance_file = open(os.path.join(other_dir, instance_filename), 'r')
    lines = instance_file.readlines()
    nb_serveurs.append(int(lines[4]))


x = list(range(0,199,6))    # rayon autour du serveur
y = nb_serveurs             # Nombre de voisin du serveur dans le rayon x (20 element)
i = 0

# triplets = []
# for rayon in x:
#     for f in range(len(files)):
#         for count in range(0, nb_serveurs[f] + 1):
#             i+=1
#             triplets.append((files[f], rayon, count))
#             os.system("C:/Users/maxim/AppData/Local/Programs/Python/Python38/python.exe test\\k_serveurs_copy.py public_online_dataset {} {} test\\results {} {}".format(files[f], i, rayon, count))
# print(i)
# exit()

r = []
c = []
for rayon in x:
    for f in range(len(files)):
        for count in range(0, nb_serveurs[f] + 1):
            r.append(rayon)
            c.append(count)

input_dir = os.path.abspath("test\\results\\")

z = []  # score 
files = []
for instance_filename in sorted(os.listdir(input_dir)):
    instance_file = open(os.path.join(input_dir, instance_filename), 'r')
    lines = instance_file.readlines()
    files.append(instance_filename)
    z.append(float(lines[1][28:]))



# fig = plt.figure()
# ax = plt.axes(projection = "3d")

# ax.plot3D(r,c,z)
# ax.set_xlabel("Rayon autour du serveur")
# ax.set_ylabel("Nombre de voisin du serveur dans le rayon x")
# ax.set_zlabel("Score ")

# plt.show()

score_min = min(z)
rayon_opti = r[z.index(score_min)]
count_opti = c[z.index(score_min)]
print(files[z.index(score_min)])
print(z.index(score_min))
print(rayon_opti)
print(count_opti)
print(score_min)


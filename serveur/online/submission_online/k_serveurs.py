import sys, os, time
from statistics import mean

# calcul de distance de manhattan entre 2 points, un point est un tuple (x, y)
def manhatan_distance (p_1, p_2):
    return abs(p_1[0] - p_2[0]) + abs(p_1[1] - p_2[1]) 

def count_voisin(pos_serveurs, s, rayon):
    c = 0
    for k in pos_serveurs.keys():
        if k == s:
            continue
        else:
            if manhatan_distance(pos_serveurs[k], pos_serveurs[s]) < rayon:
                c += 1
    return c

def online_k_serveurs(pos_sites, pos_serveurs, demande):
    # S'il y a deja un serveur sur le site, on utilise celui ci
    for k in pos_serveurs.keys():
        if pos_serveurs[k] == pos_sites[demande]:
            return k
    
    rayon = 1   # rayon de recherce autour de la demande
    prox = {}   # proximite de la demande
    d_max = manhatan_distance((0,0), (99,99))   # distance max possible sur la grille

    # Tant que le rayon de recherche autour de la demande est inferieur a la distance max et que tout les serveurs ne sont pas dans la proximite de la demande
    while rayon < d_max and len(prox) < len(pos_serveurs):
        l = len(prox)
        not_intersec = [i for i in pos_serveurs.keys() if i not in prox.keys()] # Liste des serveurs qui ne sont pas dans l'ancien rayon de recherche

        # Pour tous les serveurs qui ne sont pas dans l'ancien rayon
        for k in not_intersec:
            # Si la distance est inferieur au rayon, on ajoute le serveur a la proximite de la demande
            if manhatan_distance(pos_serveurs[k], pos_sites[demande]) < rayon:
                prox[k] = pos_serveurs[k]

        # Si on a ajouter des serveurs dans la proximite
        if l != len(prox):
            # Pour tous ces serverus, si ils ont au moins 1 voisin dans un rayon de 6 (a optimiser), on renvoie ce serveur
            for k in prox.keys():
                if count_voisin(pos_serveurs, k, 6) > 0:
                    return k
        # Si on a rien renvoye, on augmente de rayon de recherche
        rayon += 1
    
    # On est sorti de la boucle sans rien renvoyer, donc tous le serveurs sont isoles, donc on renvoie le serveur le plus proche
    min_d = min([ ( manhatan_distance(pos_sites[demande], pos_serveurs[k]), k ) for k in pos_serveurs.keys()])
    return min_d[1]

##############################################################
#### LISEZ LE MANIFEST et NE PAS MODIFIER LE CODE SUIVANT ####
##############################################################
if __name__=="__main__":
    input_dir = os.path.abspath(sys.argv[1])
    output_dir = os.path.abspath(sys.argv[2])
    
    # un repertoire des graphes en entree doit être passé en parametre 1
    if not os.path.isdir(input_dir):
	    print(input_dir, "doesn't exist")
	    exit()

    # un repertoire pour enregistrer les dominants doit être passé en parametre 2
    if not os.path.isdir(output_dir):
	    print(output_dir, "doesn't exist")
	    exit()       
	
    # fichier des reponses depose dans le output_dir et annote par date/heure
    output_filename = 'answers_{}.txt'.format(time.strftime("%d%b%Y_%H%M%S", time.localtime()))             
    output_file = open(os.path.join(output_dir, output_filename), 'w')

    ratios = []
    
    for instance_filename in sorted(os.listdir(input_dir)):
        # importer l'instance depuis le fichier (attention code non robuste)
        instance_file = open(os.path.join(input_dir, instance_filename), "r")
        lines = instance_file.readlines()
        
        cout_opt = int(lines[1])
        k = int(lines[4])
        
        idx_demande = lines.index('# demandes\n')
        sites = {}
        for i in range(7,idx_demande-1):
            coords = lines[i].split()
            sites[i-7] = (int(coords[0]), int(coords[1]))

        demandes = [int(d) for d in lines[idx_demande+1].split()]
                
        # lancement de l'algo online 10 fois et calcul du meilleur cout
        nb_runs = 10
        meilleur_cout = float('inf')
        for _ in range(nb_runs):
            cout_online = 0
            serveurs = {i:(0,0) for i in range(k)} #tous les serveurs à la position 0,0 au départ
            for d in demandes:
                r = online_k_serveurs(sites.copy(), serveurs.copy(), d)
                cout_online += manhatan_distance(serveurs[r], sites[d])
                serveurs[r] = sites[d]
            
            meilleur_cout = min(meilleur_cout, cout_online)

        ratios.append(meilleur_cout/cout_opt)

        # ajout au rapport
        output_file.write(instance_filename + ': optimal: {}, online: {}, ratio: {}\n'.format(cout_opt, meilleur_cout, ratios[-1]))

    output_file.write('score (moyenne des ratios) :' + str(mean(ratios)))

    output_file.close()


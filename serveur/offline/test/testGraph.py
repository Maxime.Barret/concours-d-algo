import networkx as nx
import os

# calcul de distance de manhattan entre 2 points, un point est un tuple (x, y)
def manhatan_distance (p_1, p_2):
    return abs(p_1[0] - p_2[0]) + abs(p_1[1] - p_2[1]) 


def getPath(mincostFlow, v = "s", end = "t"):
    path = [v]
    if v != end:
        for s in mincostFlow[v]:
            if mincostFlow[v][s] == 1:
                path.append(getPath(mincostFlow, s))
    return path

def reponse(paths, N):
    liste = ["" for i in range(N)]
    for i in range(len(paths)):
        if paths[i] == "s":
            continue
        else:
            sommet = paths[i]
            serveur = sommet[0]
            sommet = sommet[1]
            while sommet[0] != "t":
                if "_" in sommet[0]:
                    sommet = sommet[1]
                else:
                    num = int(sommet[0][1:])
                    liste[num - 1] = int(serveur[1:]) - 1
                    sommet = sommet[1]

    return liste

output_dir = "offline\\test"
output_filename = "log.txt"
output_dir = os.path.abspath(output_dir)
output_file = open(os.path.join(output_dir, output_filename), 'w')

def offline_k_serveurs(pos_sites, k, demandes):

    N = len(demandes)
    G = nx.DiGraph()

    liste_edges = []
    for i in range(0, k):
        liste_edges.append( ("s", "s{}".format(i+1), {"capacity" : 1, "weight" : 0}) )    # irriguer le reseau

        # envoyer un serveur servur sa premiere requete
        for j in range(0, N):
            distance = manhatan_distance((0,0), pos_sites[demandes[j]])
            liste_edges.append( ("s{}".format(i+1), "r{}".format(j+1), {"capacity" : 1, "weight" : distance}) ) 
        
        liste_edges.append( ("s{}".format(i+1), "t", {"capacity" : 1, "weight" : 0}) )  # serveurs qui ne servent aucun client

    for j in range(0,N):
        liste_edges.append( ("r{}".format(j+1), "r_{}".format(j+1), {"capacity" : 1, "weight" : -100000000000000000000000000000}) ) # pour qu’un serveur puisse aller servir une autre demande apres 
                                                                                                                                    # avoir termine de s’occuper de la requete rj

        # pour qu’un serveur puisse aller servir la requete rj depuis l’endroit ou il a effectue l’intervention ri
        for i in range(0,N):
            if i < j:
                distance = manhatan_distance(pos_sites[demandes[i]], pos_sites[demandes[j]])
                liste_edges.append( ("r_{}".format(i+1), "r{}".format(j+1), {"capacity" : 1, "weight" : distance}) )
        
        liste_edges.append( ("r_{}".format(j+1), "t", {"capacity" : 1, "weight" : 0}) ) # evacuer le flot apres avoir servis les requetes
    
    G.add_edges_from( liste_edges )

    mincostFlow = nx.max_flow_min_cost(G, "s", "t")
    output_file.write("{}\n".format(mincostFlow))
    paths= getPath(mincostFlow)

    planning = reponse(paths, len(demandes))

    return planning

input_dir = "offline\\public_offline_dataset"
instance_filename = "instance_N200_OPT214.inst"
input_dir = os.path.abspath(input_dir)
instance_file = open(os.path.join(input_dir, instance_filename), "r")



lines = instance_file.readlines()
cout_opt = int(lines[1])
k = int(lines[4])
print("k", k)

idx_demande = lines.index('# demandes\n')
sites = {}
for i in range(7,idx_demande-1):
    coords = lines[i].split()
    sites[i-7] = (int(coords[0]), int(coords[1]))

print("sites", sites)
demandes = [int(d) for d in lines[idx_demande+1].split()]
print("demandes", demandes)

# lancement de l'algo offline
reponses = offline_k_serveurs(sites, k, demandes)

output_file.write("\n\nplanning : ")
output_file.write("\n{}".format(reponses))

    
# calcul cout
cout_offline = 0
serveurs = {i:(0,0) for i in range(k)} #tous les serveurs à la position 0,0 au départ
for d,r in zip(demandes,reponses):
    cout_offline += manhatan_distance(serveurs[r], sites[d])
    serveurs[r] = sites[d]

print(cout_offline)


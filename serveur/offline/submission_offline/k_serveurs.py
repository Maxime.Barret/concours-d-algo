import sys, os, time
import networkx as nx

# calcul de distance de manhattan entre 2 points, un point est un tuple (x, y)
def manhatan_distance (p_1, p_2):
    return abs(p_1[0] - p_2[0]) + abs(p_1[1] - p_2[1]) 

def getPath(mincostFlow, v = "s", end = "t"):
    path = [v]
    if v != end:
        for s in mincostFlow[v]:
            if mincostFlow[v][s] == 1:
                path.append(getPath(mincostFlow, s))
    return path

def reponse(paths, N):
    liste = ["" for i in range(N)]
    for i in range(len(paths)):
        if paths[i] == "s":
            continue
        else:
            sommet = paths[i]
            serveur = sommet[0]
            sommet = sommet[1]
            while sommet[0] != "t":
                if "_" in sommet[0]:
                    sommet = sommet[1]
                else:
                    num = int(sommet[0][1:])
                    liste[num - 1] = int(serveur[1:]) - 1
                    sommet = sommet[1]

    return liste


def offline_k_serveurs(pos_sites, k, demandes):

    N = len(demandes)
    G = nx.DiGraph()

    liste_edges = []
    for i in range(0, k):
        liste_edges.append( ("s", "s{}".format(i+1), {"capacity" : 1, "weight" : 0}) )    # irriguer le reseau

        # envoyer un serveur servur sa premiere requete
        for j in range(0, N):
            distance = manhatan_distance((0,0), pos_sites[demandes[j]])
            liste_edges.append( ("s{}".format(i+1), "r{}".format(j+1), {"capacity" : 1, "weight" : distance}) ) 
        
        liste_edges.append( ("s{}".format(i+1), "t", {"capacity" : 1, "weight" : 0}) )  # serveurs qui ne servent aucun client

    for j in range(0,N):
        liste_edges.append( ("r{}".format(j+1), "r_{}".format(j+1), {"capacity" : 1, "weight" : -100000000000000000000000000000}) ) # pour qu’un serveur puisse aller servir une autre demande apres 
                                                                                                                                    # avoir termine de s’occuper de la requete rj

        # pour qu’un serveur puisse aller servir la requete rj depuis l’endroit ou il a effectue l’intervention ri
        for i in range(0,N):
            if i < j:
                distance = manhatan_distance(pos_sites[demandes[i]], pos_sites[demandes[j]])
                liste_edges.append( ("r_{}".format(i+1), "r{}".format(j+1), {"capacity" : 1, "weight" : distance}) )
        
        liste_edges.append( ("r_{}".format(j+1), "t", {"capacity" : 1, "weight" : 0}) ) # evacuer le flot apres avoir servis les requetes
    
    G.add_edges_from( liste_edges ) # ajout des arcs

    mincostFlow = nx.max_flow_min_cost(G, "s", "t") # calculer le flow du graph
    
    paths= getPath(mincostFlow) # calculer le chemin entre s et t en passant par chaque serveur

    planning = reponse(paths, len(demandes))    # definir le planning en fonction de paths

    return planning

##############################################################
#### LISEZ LE MANIFEST et NE PAS MODIFIER LE CODE SUIVANT ####
##############################################################
if __name__=="__main__":
    input_dir = os.path.abspath(sys.argv[1])
    output_dir = os.path.abspath(sys.argv[2])
    
    # un repertoire des graphes en entree doit être passé en parametre 1
    if not os.path.isdir(input_dir):
	    print(input_dir, "doesn't exist")
	    exit()

    # un repertoire pour enregistrer les dominants doit être passé en parametre 2
    if not os.path.isdir(output_dir):
	    print(output_dir, "doesn't exist")
	    exit()       
	
    # fichier des reponses depose dans le output_dir et annote par date/heure
    output_filename = 'answers_{}.txt'.format(time.strftime("%d%b%Y_%H%M%S", time.localtime()))             
    output_file = open(os.path.join(output_dir, output_filename), 'w')

    for instance_filename in sorted(os.listdir(input_dir)):
        # importer l'instance depuis le fichier (attention code non robuste)
        instance_file = open(os.path.join(input_dir, instance_filename), "r")
        lines = instance_file.readlines()
        
        cout_opt = int(lines[1])
        k = int(lines[4])
        
        idx_demande = lines.index('# demandes\n')
        sites = {}
        for i in range(7,idx_demande-1):
            coords = lines[i].split()
            sites[i-7] = (int(coords[0]), int(coords[1]))

        demandes = [int(d) for d in lines[idx_demande+1].split()]
                
        # lancement de l'algo offline
        reponses = offline_k_serveurs(sites, k, demandes)

        # calcul cout
        cout_offline = 0
        serveurs = {i:(0,0) for i in range(k)} #tous les serveurs à la position 0,0 au départ
        for d,r in zip(demandes,reponses):
            cout_offline += manhatan_distance(serveurs[r], sites[d])
            serveurs[r] = sites[d]

        # ajout au rapport
        output_file.write(instance_filename)
        if cout_opt != cout_offline:
            output_file.write(': KO expected: {}, found: {}\n'.format(cout_opt, cout_offline))
        else:
            output_file.write(': OK\n')
        
    output_file.close()

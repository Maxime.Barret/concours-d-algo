**Concours d'algorithmes**

**_Abstract_**

This repository regroups 3 projects of algorithm championships. They all consists in finding a optimized way to solve well known problems (NP) :
- Computing prime numbers factorization of large numbers
- Responding to N requests with K servers (offline and online algorithm)
- Finding the dominating set of a graph

All implementations are done with Python.


**_Résumé_**

Ce répertoire regoupe 3 projets de concours d'algorithmes. Ils consistent tous en trouver une meilleure solution à un problème connu (NP) :
- Calculer la décomposition en nombres premiers de grands nombres
- Répondre à N demandes avec K serveurs (algorithme online et offline)
- Trouver l'ensemble dominant d'un graph

Toutes les implémentations sont réalisées en Python.

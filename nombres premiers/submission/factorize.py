import sys, os


def factorize(numbers):

    dico = {}
       
    for n in numbers:
        num = n
        facto = []

        # 1er tour de wheel (tester les premiers nombres premiers 2,3 et 5)
        for i in [2,3,5]:
            while n % i == 0:
                facto.append(i)
                n //= i

        increm = [4,2,4,2,4,6,2,6]  # astuce : paterne des nombres premiers 
        i = 0
        d = 7

        # 2e tour de wheel (tester tout les nombres premier jusqu'a sqrt(n))
        while d * d <= n:
            # Tant que le nombre premier divise n, on l'ajoute a la liste de sortie et on divise n
            while n % d == 0:
                facto.append(d)
                n //= d
            
            d += increm[i]      # d devient le premier suivant
            i = (i + 1) % 8

        # Si n > 1 après les 2 tours, c'est que n est premier
        if n > 1:
            facto.append(n)
           
        dico[num] = facto
    
    return dico 

#########################################
#### Ne pas modifier le code suivant ####
#########################################
if __name__=="__main__":
    input_dir = os.path.abspath(sys.argv[1])
    output_dir = os.path.abspath(sys.argv[2])
    
    # un repertoire des fichiers en entree doit être passé en parametre 1
    if not os.path.isdir(input_dir):
	    print(input_dir, "doesn't exist")
	    exit()

    # un repertoire pour enregistrer les résultats doit être passé en parametre 2
    if not os.path.isdir(output_dir):
	    print(output_dir, "doesn't exist")
	    exit()       

     # Pour chacun des fichiers en entrée 
    for data_filename in sorted(os.listdir(input_dir)):
        # importer la liste des nombres
        data_file = open(os.path.join(input_dir, data_filename), "r")
        numbers = [int(line) for line in data_file.readlines()]        
        
        # decomposition en facteurs premiers
        D = factorize(numbers)

        # fichier des reponses depose dans le output_dir
        output_filename = 'answer_{}'.format(data_filename)             
        output_file = open(os.path.join(output_dir, output_filename), 'w')
        
        # ecriture des resultats
        for (n, primes) in D.items():
            output_file.write('{} {}\n'.format(n, primes))
        
        output_file.close()

    

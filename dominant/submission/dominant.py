import sys, os, time
import networkx as nx

def test_dominant(g,dominant):
    alone = []
    for v in list(g.nodes):
        if v not in dominant:
            b = False
            for vv in list(g.adj[v]):
                if vv in dominant:
                    b = True
                    break
            if not b:
                alone.append(v)
    if len(alone) != 0:
        print(" -- ERR -- Le sous ensemble {} n'est pas un dominant :".format(dominant))
        print("{} tout seul".format(alone))
        return False, alone
    else:
        print(" -- OK -- Le sous ensemble {} est un dominant :".format(dominant))
        return True, alone


def is_cyclique(g):
    """
    Verifie si le graphe est cyclique
    g est le graph sous le format de networkx
    renvoie 1 si oui, 0 sinon
    """
    V = list(g.nodes)
    index = 0;
    while index < len(V) and len(list(g.adj[V[index]])) == 2:   # les noeuds ont tous 2 voisins, sinon pas cyclique
        index += 1
    if index == len(V):
        return 1
    else:
        return 0


def dominantCyclique(g):
    """
    Fonction executée pour renvoyer le dominant si le graphe est cyclique
    renvoie le dominant du graphe g
    """
    
    def placementCyclique(g):
        """
        Réarrange la liste des noeuds dans l'odre du cycle du graphe
        ret : liste réarrangée
        """
        V = list(g.nodes)
        ret = [V[0]]
        for i in range(len(V) - 1):
            voisinage = list(g.adj[ret[-1]])
            if voisinage[0] not in ret:
                ret.append(voisinage[0])
            else:
                ret.append(voisinage[1])
        return ret 
    
    D = []
    V = placementCyclique(g)
    for i in range(0, len(V) // 3 + 1):
        D.append(V[i*3])
    return D
    
def isPaw(g):
    """
    
        TEMPORAIRE

    """
    return g.number_of_edges() in [16,96,196]    # for now



def dominantPaw(g):
    """
    Fonction de calucl d'un dominant d'un graph patte
    
    """

    def delete(elem, liste):
        if elem in liste:
            index = liste.index(elem)
            del liste[index]
        return liste
        
    def get_tuple(i,x,y,z):
        """
        Implementation de la structure switch sur les tuples
        
        """
        switcher = {
         0 : (x,y,z),
         1 : (y,z,x),
         2 : (z,x,y),
         3 : (x,z,y),
         4 : (y,x,z),
         5 : (z,y,x)
         }
        return switcher.get(i, "invalid")
    
    
    def good_tuple(liste,x,y,z):
        """
        Renvoie liste += (x,y,z) si aucune des combinaisons de x,y et z est present dans liste
                liste sinon
       
        liste : liste de triplet
        x,y,z : 3 variables
        i : argument de parcours des combinaisons de x,y et z
        """
        for j in range(0, 6):
            if get_tuple(j, x, y, z) in liste:
                break
        if j == 5:
            liste.append((x,y,z))
        return liste
    
    
    def gestion_egalite(g, dominant, egalite):
        """
        Supprime le(s) sommet(s) superflux du dominant en cas d'egalite dans le HOST
        
        """
        
        for s in egalite:
            selette = 0
            voisinage = list(g.adj[s])
            copie_d = list(dominant)
            copie_d = delete(s, copie_d)
            for v in voisinage:
                if v not in dominant:
                    voisinage_v = list(g.adj[v])
                    compteur = 0
                    for voisin_v in voisinage_v:
                        if voisin_v not in copie_d:
                            compteur += 1
                    if compteur != len(voisinage_v):
                        selette += 1
                else:
                    selette += 1
            if selette == len(voisinage):
                delete(s, dominant)
        return dominant
        
        
    
    def traitement_aberation(histo):
        """
        Traite les aberations* de l'histogramme seuille des occurences des sommets dans les triangles
        histo : dictionnaire
        Renvoie le dominant du graph
        
        *aberation = cle a valeur nul, n'appartient pas a un triangle
        """
        dominant = list(histo.keys())
    
        for a in aberation:
            b = False
            for v in list(g.adj[a]):
                if v in dominant:
                    b = True
                    break
            if not b:
                dominant.append(a)
        return dominant


    ##########                            ##########
    ##########  Execution principale PAW  ##########
    ##########                            ##########
    ##########                            ##########
    
    V = list(g.nodes)
    triangles = []      ## liste des trangles (triplets)
    histo = {}          ## HOST
    moy_voisin = 0      ## seil du HOST
    
    
    ##### Recherche des triangles #####
    
    for sommet1 in V:
        histo[sommet1] = 0
        voisin_sommet1 = list(g.adj[sommet1])
        moy_voisin += len(voisin_sommet1)
        if len(voisin_sommet1) == 2:
            voisin_sommet2 = list(g.adj[voisin_sommet1[0]])
            if voisin_sommet1[1] in voisin_sommet2:
                triangles = good_tuple(triangles,sommet1,voisin_sommet1[0], voisin_sommet1[1])
        elif len(voisin_sommet1) > 1:
            has_broke = False;
            for sommet2 in voisin_sommet1:
                voisin_sommet2 = list(g.adj[sommet2])
                for sommet3 in voisin_sommet2:
                    if sommet3 in voisin_sommet1:
                        triangles = good_tuple(triangles,sommet1, sommet2, sommet3)
                        has_broke = True
                        break
                if has_broke:
                    break
                
    ##### Traitement des triangles #####
                
    ### Etablissement de l'histogramme des occurences des sommets dans les triangles (HOST) ###
    
    moy_voisin /= len(V)
    for t in triangles:
        histo[t[0]] += 1
        histo[t[1]] += 1
        histo[t[2]] += 1
    
    ### Seuillage du HOST ###
    
    keys = list(histo.keys())
    aberation = []
    for k in keys:
        if histo[k] < int(moy_voisin) + 1:
            if histo[k] == 0:
                aberation.append(k)
            del histo[k]

    
    
    ### Traitement des aberations du HOST seuille ###
    
    dominant = traitement_aberation(histo)  ## premiere version du dominant (avec egalites s'il y a)
            
    
    ### Gestion des egalites dans le HOST seuille ###
    
    egalite = []
    for k in histo.keys():
        for kp in histo.keys():
            if k != kp and k not in egalite and kp not in egalite and histo[k] == histo[kp]:
                egalite.append(k)
                egalite.append(kp)

    
    if len(egalite) != 0:
        dominant = gestion_egalite(g, dominant, egalite)
        
    succeed, alone = test_dominant(g, dominant)
    if not succeed:
        for v in alone:
            voisin = list(g.adj[v])
            for vv in voisin:
                if vv not in dominant:
                    dominant.append(vv)
                    break
        print("Modification du dominant")
        test_dominant(g, dominant)
    
    return dominant


###########################################################################################################
########################################### FONCTION PRINCIPALE ###########################################
###########################################################################################################


def dominant(g):
    """
        A Faire:         
        - Ecrire une fonction qui retourne le dominant du graphe non dirigé g passé en parametre.
        - cette fonction doit retourner la liste des noeuds d'un petit dominant de g

        :param g: le graphe est donné dans le format networkx : https://networkx.github.io/documentation/stable/reference/classes/graph.html

    """
    D = []
    
    if is_cyclique(g):          # specifique pour les graphes de type cyclique
        D = dominantCyclique(g)
    elif isPaw(g):
        D = dominantPaw(g)
    else:
        R = list(g.nodes)
        while len(R) > 0:
            degrees = [g.degree[i] for i in R]
            index_max_degrees = degrees.index(max(degrees))
            s = R[index_max_degrees]
            voisinage = list(g.adj[R[index_max_degrees]])
            
            D = D + [s]
            liste_a_soustraire = [s] + voisinage
            #R = actualisation(R, liste_a_soustraire)
            g.remove_nodes_from(liste_a_soustraire)
            R = list(g.nodes)
    test_dominant(g, D)  
    return D #g.nodes # pas terrible :) mais c'est un dominant





#########################################
#### Ne pas modifier le code suivant ####
#########################################
if __name__=="__main__":
    input_dir = os.path.abspath(sys.argv[1])
    output_dir = os.path.abspath(sys.argv[2])
    
    # un repertoire des graphes en entree doit être passé en parametre 1
    if not os.path.isdir(input_dir):
	    print(input_dir, "doesn't exist")
	    exit()

    # un repertoire pour enregistrer les dominants doit être passé en parametre 2
    if not os.path.isdir(output_dir):
	    print(output_dir, "doesn't exist")
	    exit()       

    # fichier des reponses depose dans le output_dir et annote par date/heure
    output_filename = 'answers_{}.txt'.format(time.strftime("%d%b%Y_%H%M%S", time.localtime()))             
    output_file = open(os.path.join(output_dir, output_filename), 'w')

    for graph_filename in sorted(os.listdir(input_dir)):
        print(graph_filename)
        # importer le graphe
        g = nx.read_adjlist(os.path.join(input_dir, graph_filename))

        # calcul du dominant
        D = sorted(dominant(g), key=lambda x: int(x))

        # ajout au rapport
        output_file.write(graph_filename)
        for node in D:
            output_file.write(' {}'.format(node))
        output_file.write('\n')
        
    output_file.close()

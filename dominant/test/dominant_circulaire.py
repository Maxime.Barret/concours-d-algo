import os
import networkx as nx

def placementCirculaire(g):
    V = list(g.nodes)
    ret = [V[0]]
    for i in range(len(V) - 1):
        voisinage = list(g.adj[ret[-1]])
        if voisinage[0] not in ret:
            ret.append(voisinage[0])
        else:
            ret.append(voisinage[1])
    return ret 



input_dir = "..\public_dataset" 
graph_filename = "01_10_bin.graph"
g = nx.read_adjlist(os.path.join(input_dir, graph_filename))

nx.draw_networkx(g)

V = list(g.nodes)
index = 0;
circulaire = 0
while index < len(V) and len(list(g.adj[V[index]])) == 2:
    index += 1
if index == len(V):
    circulaire = 1
    print("circulaire")
else:
    print("non circulaire")

D = []

if circulaire:
    V = placementCirculaire(g)
    for i in range(0, len(V) // 3 + 1):
        D.append(V[i*3])

print(D)
            
        
import os
import networkx as nx

def test_dominant(g,dominant):
    alone = []
    for v in list(g.nodes):
        if v not in dominant:
            b = False
            for vv in list(g.adj[v]):
                if vv in dominant:
                    b = True
                    break
            if not b:
                alone.append(v)
    if len(alone) != 0:
        print(" -- ERR -- Le sous ensemble {} n'est pas un dominant :".format(dominant))
        print("{} tout seul".format(alone))
        return False, alone
    else:
        print(" -- OK -- Le sous ensemble {} est un dominant :".format(dominant))
        return True, alone

def delete(elem, liste):
    if elem in liste:
        index = liste.index(elem)
        del liste[index]
    return liste
    
def get_tuple(i,x,y,z):
    """
    Implementation de la structure switch sur les tuples
    
    """
    switcher = {
     0 : (x,y,z),
     1 : (y,z,x),
     2 : (z,x,y),
     3 : (x,z,y),
     4 : (y,x,z),
     5 : (z,y,x)
     }
    return switcher.get(i, "invalid")


def good_tuple(liste,x,y,z):
    """
    Renvoie liste += (x,y,z) si aucune des combinaisons de x,y et z est present dans liste
            liste sinon
   
    liste : liste de triplet
    x,y,z : 3 variables
    i : argument de parcours des combinaisons de x,y et z
    """
    for j in range(0, 6):
        if get_tuple(j, x, y, z) in liste:
            break
    if j == 5:
        liste.append((x,y,z))
    return liste


def gestion_egalite(g, dominant, egalite):
    """
    Supprime le(s) sommet(s) superflux du dominant en cas d'egalite dans le HOST
    
    """
    
    for s in egalite:
        selette = 0
        voisinage = list(g.adj[s])
        copie_d = list(dominant)
        copie_d = delete(s, copie_d)
        for v in voisinage:
            if v not in dominant:
                voisinage_v = list(g.adj[v])
                compteur = 0
                for voisin_v in voisinage_v:
                    if voisin_v not in copie_d:
                        compteur += 1
                if compteur != len(voisinage_v):
                    selette += 1
            else:
                selette += 1
        if selette == len(voisinage):
            print("\t{} est enleve des sommets dominant".format(s))
            delete(s, dominant)
    return dominant
    
    

def traitement_aberation(histo,aberation):
    """
    Traite les aberations* de l'histogramme seuille des occurences des sommets dans les triangles
    histo : dictionnaire
    Renvoie le dominant du graph
    
    *aberation = cle a valeur nul, n'appartient pas a un triangle
    """
    dominant = list(histo.keys())

    for a in aberation:
        b = False
        for v in list(g.adj[a]):
            if v in dominant:
                b = True
                break
        if not b:
            print("[ABERATION] rajout de {} dans le dominant".format(a))
            dominant.append(a)
    return dominant





##########                        ##########
##########  Execution principale  ##########
##########                        ##########
##########                        ##########


SEUILLAGE = True
ABERATION = True

input_dir = "..\public_dataset" 
graph_filename = "23_50_paw.graph"
print(graph_filename)
g = nx.read_adjlist(os.path.join(input_dir, graph_filename))

nx.draw_networkx(g)

V = list(g.nodes)
triangles = []      ## liste des trangles (triplets)
histo = {}          ## HOST
moy_voisin = 0      ## seil du HOST
print("{} arretes".format(g.number_of_edges()))

##### Recherche des triangles #####

for sommet1 in V:
    histo[sommet1] = 0
    voisin_sommet1 = list(g.adj[sommet1])
    moy_voisin += len(voisin_sommet1)
    if len(voisin_sommet1) == 2:
        voisin_sommet2 = list(g.adj[voisin_sommet1[0]])
        if voisin_sommet1[1] in voisin_sommet2:
            triangles = good_tuple(triangles,sommet1,voisin_sommet1[0], voisin_sommet1[1])
    elif len(voisin_sommet1) > 1:
        has_broke = False;
        for sommet2 in voisin_sommet1:
            voisin_sommet2 = list(g.adj[sommet2])
            for sommet3 in voisin_sommet2:
                if sommet3 in voisin_sommet1:
                    triangles = good_tuple(triangles,sommet1, sommet2, sommet3)
                    has_broke = True
                    break
            if has_broke:
                break

print("{} triangles".format(len(triangles)))
print(triangles)
##### Traitement des triangles #####
            
### Etablissement de l'histogramme des occurences des sommets dans les triangles (HOST) ###

moy_voisin /= len(V)
for t in triangles:
    histo[t[0]] += 1
    histo[t[1]] += 1
    histo[t[2]] += 1
    
print("moyenne des nb de voisin : {}".format(moy_voisin))
print("Histo des apparitions des sommets dans les triangles : ")
print(histo)

### Seuillage du HOST ###

keys = list(histo.keys())
aberation = []
for k in keys:
    if histo[k] < int(moy_voisin) + 1:
        if histo[k] == 0 and ABERATION:
            aberation.append(k)
        if SEUILLAGE:
            del histo[k]

print("Histo seuillé à {} : ".format(int(moy_voisin) + 1))
print(histo)



### Traitement des aberations du HOST seuille ###

if ABERATION:
    dominant = traitement_aberation(histo,aberation)  ## premiere version du dominant (avec egalites s'il y a)
else:
    dominant = list(histo.keys())



### Gestion des egalites dans le HOST seuille ###

egalite = []
for k in histo.keys():
    for kp in histo.keys():
        if k != kp and k not in egalite and kp not in egalite and histo[k] == histo[kp]:
            egalite.append(k)
            egalite.append(kp)

print("Liste des egalites")
print(egalite)

if len(egalite) != 0:
    dominant = gestion_egalite(g, dominant, egalite)

succeed, alone = test_dominant(g, dominant)
if not succeed:
    for v in alone:
        voisin = list(g.adj[v])
        for vv in voisin:
            if vv not in dominant:
                dominant.append(vv)
                break
test_dominant(g, dominant)
    

    
    

